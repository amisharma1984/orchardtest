<html>
    <head>
        <title>PHP Test</title>
    </head>
    <body>
    <?php 
    // PHP Program to show
    // CRACKLE, SNAP OR POP
    for($number= 1; $number<=100; $number++) {
        if ($number%3 == 0 && $number%5 == 0) {
            echo "POP";
        }elseif($number%3 == 0) {
            echo "SNAP"; 
        } elseif($number%5 == 0) {
            echo "CRACKLE";
        } else {
            echo $number;
        }
    }
    ?> 
    </body>
</html>