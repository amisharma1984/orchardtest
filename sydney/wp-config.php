<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sydney');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-:ZRFi[Rg..n^>(4A]2a[%>R 1YgN+BK>g#3mfXl1yPK.7rOQ],f2(.]hUE@-B;j');
define('SECURE_AUTH_KEY',  '>XtVlA@CPR+xMSuuCi.J^)%vzn0+)ns@_%ZMo*ZO8Zq&E*n_qZsz2H}ANPGdXc~T');
define('LOGGED_IN_KEY',    'Qe0{/ZP}xV<hgA/?c3&ajS*ZA]l~DAUICunoX-F0E7urH}YY Wk4NHm0Zc<QDe>N');
define('NONCE_KEY',        'jy$M7>wmUV/YLl4d/`(GQ!X=J*bPMM30+XD?G}+Ea{:NeY|WGQpXzu}hp/l1/!A@');
define('AUTH_SALT',        '(qfz&S/i1;BAiC`e,|qV=g#oevN|wp/]kGWlX@ofmCO}$o3JvIT*+EB$g73>|AZV');
define('SECURE_AUTH_SALT', 'X|O@dc<!$3%WP9w~2x^xS5Ur2owe%&@;pRTtCs1~s_VGB:BK0C}PWfHb6M.}Rmbm');
define('LOGGED_IN_SALT',   '#aS&bEt 4|%~f^}<0}zll2XU2:i)Jz)/xWiHWi=*TqH:)rfMUaN,9o%o~#~:P^iO');
define('NONCE_SALT',       'B4UNBd$;!htcrAoc!,%4y2hA 6`V@PV)<)u9k$yvkGRD{n$Ma0lX#X^s^RckOyV(');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
