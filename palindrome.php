<html>
    <head>
        <title>PHP Test</title>
    </head>
    <body>
        <form method="post" action="palindrome.php">
            <input type="text" name="string">
            <input type="submit" name="submit" value="Submit">
        </form>
    <?php 
    // PHP Program to check 
    // whether the given string
    // is Palindrome or not.
        if($_SERVER["REQUEST_METHOD"] == "POST") {  
            $stringToCheck = $_POST['string'];
            if (empty($stringToCheck)) {
                echo "UNDETERMINED";
                exit;
            }
            //Remove Spaces
            $stringToCheck = str_replace(' ', '', $stringToCheck);

            //Remove Special Characters
            $stringToCheck = preg_replace('/[^A-Za-z0-9-]/', '', $stringToCheck);

            //change to lowerCase
            $stringToCheck = strtolower($stringToCheck);

            //reverse the stringToCheck
            $reverse = strrev($stringToCheck);

            if ($stringToCheck == $reverse) {
                echo "TRUE";
            } 
            else {
                echo "FALSE";
            }
        }
    ?> 
    </body>
</html>