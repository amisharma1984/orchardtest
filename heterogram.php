<?php
 
function isHeterogram($s)
{

    $occurrences = prepareOccurrences($s);
    $no_occurrences = array_values($occurrences);
    $unique_occurrences = array_unique($no_occurrences);
    
    if($unique_occurrences[0] == 1 && count($unique_occurrences) == 1) {
        return true;
    }

    return false;

}


function isIsogram($s) {

    $occurrences = prepareOccurrences($s);
    $no_occurrences = array_values($occurrences);
    $unique_occurrences = array_unique($no_occurrences);

    if($unique_occurrences[0] > 1 && count($unique_occurrences) == 1) {
        return true;
    }

    return false;

}

function prepareOccurrences($s) {
    $chars = str_split($s);
    $occurrences = [];

    if(count($chars) > 0) {
        foreach ($chars as $key => $value) {
            $value = strtolower($value);

            if(isset($occurrences[$value])) {
                $occurrences[$value] += 1;
            }
            else {
                $occurrences[$value] = 1;
            }
        }
    }

    return $occurrences;
}
 

$result = '';
if(isset($_GET['test_string'])) {
    $result = instagram($_GET['test_string']);
}

function instagram ($str) {

    $isHeterogram = isHeterogram($str);
    $isIsogram = isIsogram($str);

    if($isHeterogram) {
        $result = 'HETEROGRAM';
    } elseif ($isIsogram) {
        $result = 'ISOGRAM';
    } else {
        $result = 'NOTAGRAM';
    }

    return $result;
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Instagram Test</title>
</head>
<body>

    <form name="instagram" action="" method="GET">
        <input type="text" name="test_string" value="<?= (isset($_GET['test_string'])?$_GET['test_string']:'') ?>">
        <input type="submit" name="submit" value="Submit">
    </form>

    <p><?= $result; ?></p>

</body>
</html>